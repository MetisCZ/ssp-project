"""
URL configuration for EvidenceMHD project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from evidence import views
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index, name='index'),
    path('register', views.register, name='register'),
    path('depart/<int:depart_id>/', views.depart, name='depart'),
    path('depart/<int:depart_id>/addcomment', views.addcomment, name='addcomment'),
    path('depart/search/<str:word>/<int:sid1>.<int:sid2>', views.departlivesearch, name='departlivesearch'),
    path('depart/search', views.departsearch, name='departsearch'),
    path('user/<int:user_id>/', views.user, name='user'),
    path('user/search', views.usersearch, name='usersearch'),
    path('stop/search', views.stopsearch, name='stopsearch'),
    path('vehicle/<int:vehicle_id>/', views.vehicle, name='vehicle'),
    path('vehicle/search', views.vehiclesearch, name='vehiclesearch'),
    path('vehicle/types', views.vehicletypes, name='vehicletypes'),
    path('login', views.login, name='login'),
    path('logout', views.logout, name='logout'),
]
