from django.db import models

class Manufacturer(models.Model):
    name = models.CharField(max_length=32)
    
    def __str__(self):
        return self.name

class VehicleType(models.Model):
    manufacturer = models.ForeignKey(Manufacturer, on_delete=models.RESTRICT)
    name = models.CharField(max_length=32)
    
    def __str__(self):
        return f'{self.manufacturer} {self.name}'

class Carrier(models.Model):
    name = models.CharField(max_length=32)
    
    def __str__(self):
        return self.name

class Vehicle(models.Model):
    carrier = models.ForeignKey(Carrier, on_delete=models.RESTRICT)
    type = models.ForeignKey(VehicleType, on_delete=models.RESTRICT)
    reg_num = models.CharField(max_length=7)
    state = models.SmallIntegerField()
    datetime_added = models.DateTimeField('date added', auto_now_add=True)
    manufactured_year = models.SmallIntegerField(null=True, blank=True)
    last_seen = models.DateTimeField('last seen date', null=True, blank=True)
    SPZ = models.CharField(max_length=7, null=True, blank=True)
    
    def __str__(self):
        return self.reg_num
    
class Stop(models.Model):
    name = models.CharField(max_length=64)
    
    def __str__(self):
        return self.name
    
class Depart(models.Model):
    first_stop = models.ForeignKey(Stop, on_delete=models.RESTRICT, related_name='fstop')
    actual_stop = models.ForeignKey(Stop, on_delete=models.RESTRICT, related_name='astop')
    last_stop = models.ForeignKey(Stop, on_delete=models.RESTRICT, related_name='lstop')
    first_vehicle = models.ForeignKey(Vehicle, on_delete=models.RESTRICT)
    line = models.CharField(max_length=6)
    route = models.IntegerField()
    date = models.DateField(auto_now_add=True)
    datetime_start = models.DateTimeField(auto_now_add=True)
    datetime_actual = models.DateTimeField(auto_now_add=True)
    actual_delay = models.IntegerField(null=True, blank=True)
    
    def __str__(self):
        return f'{self.line}/{self.route} on {self.date.strftime("%d.%m.%Y")}'
    
class User(models.Model):
    username = models.CharField(max_length=24)
    email = models.EmailField(max_length=320)
    role = models.SmallIntegerField(default=0)
    password_hash = models.CharField(max_length=256)
    last_login = models.DateTimeField(auto_now_add=True)
    #authenticated = db.Column(db.Boolean, default=False)

    USERNAME_FIELD = 'email'

    def is_active(self):
        return True
    
    def get_id(self):
        return self.email
    
    is_authenticated = True

    def is_anonymous(self):
        # False as we do not support annonymity
        return False
    
    def __str__(self):
        return f'{self.username}'
    
class Comment(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    depart = models.ForeignKey(Depart, on_delete=models.CASCADE)
    datetime_added = models.DateTimeField(auto_now_add=True)
    content = models.TextField()
    
    def __str__(self):
        return f'{self.user.username} -> {self.depart} wrote {self.content}'
    
class SetVehicle(models.Model):
    vehicle = models.ForeignKey(Vehicle, on_delete=models.RESTRICT)
    depart = models.ForeignKey(Depart, on_delete=models.CASCADE)
    position = models.SmallIntegerField()
    
    def __str__(self):
        return f'{self.vehicle} in {self.depart}'
    
class RegNumberHistory(models.Model):
    vehicle = models.ForeignKey(Vehicle, on_delete=models.CASCADE)
    date_valid_from = models.DateTimeField()
    date_valid_to = models.DateTimeField(auto_now_add=True)
    reg_num = models.CharField(max_length=7)
    
    def __str__(self):
        return f'{self.vehicle.reg_num} was {self.reg_num}'
    
# python manage.py makemigrations evidence
# python manage.py migrate
# python manage.py runserver

# python manage.py shell

# d1 = Depart.objects.get(pk=1)
# Comment.objects.filter(depart=d1)
# d1.comment_set.all()
# d1.comment_set.filter(content__contains='first')