from django.shortcuts import render, get_object_or_404, redirect
from django.contrib import messages
from django.contrib.auth.hashers import make_password
from django.http import HttpResponse
from django.urls import reverse
from django.db.models import Q
from django.contrib.auth import authenticate
from .models import Depart, Comment, User, SetVehicle, Vehicle, RegNumberHistory, Stop, VehicleType
from .forms import CommentForm, UserRegistrationForm, UserSearchForm, StopSearchForm, VehicleSearchForm, UserLoginForm


def index(request):
    departs = Depart.objects.all()
    users = User.objects.all()
    vehicles = Vehicle.objects.all()
    comments = Comment.objects.all()

    try:
        user = User.objects.get(pk=request.session['user'])
    except User.DoesNotExist and KeyError:
        user = None
    
    return render(request, 'evidence/index.html', {'user': user, 'departs': len(departs), 'users': len(users), 'vehicles': len(vehicles), 'comments': len(comments)})


def login(request):
    if request.method == 'POST':
        form = UserLoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(request, username=username, password=password)
            
            if user is not None:
                request.session['user'] = user.id
                return redirect('index')
            else:
                form.add_error(None, 'Invalid login')
    else:
        form = UserLoginForm()

    try:
        user = User.objects.get(pk=request.session['user'])
    except User.DoesNotExist and KeyError:
        user = None

    return render(request, 'evidence/login.html', {'user': user, 'form': form})


def logout(request):
    try:
        del request.session['user']
    except KeyError:
        pass
    return render(request, 'evidence/logout.html', {'user': None})


def register(request):
    if request.method == 'POST':
        form = UserRegistrationForm(request.POST)
        if form.is_valid():
            user = User()
            user.username = form.data.get('username')
            user.email = form.data.get('email')
            user.password_hash = make_password(form.data.get('password1'))
            user.save()

            messages.success(request, f'Your account has been created. You can log in now!')    
            return redirect('index')
    else:
        form = UserRegistrationForm()

    try:
        user = User.objects.get(pk=request.session['user'])
    except User.DoesNotExist and KeyError:
        user = None

    return render(request, 'evidence/register.html', {'user': user, 'form': form})


def depart(request, depart_id):
    depart = get_object_or_404(Depart, pk=depart_id)
    comments = Comment.objects.filter(depart=depart)
    sets = SetVehicle.objects.filter(depart=depart)
    comment_form = CommentForm()

    try:
        user = User.objects.get(pk=request.session['user'])
    except User.DoesNotExist and KeyError:
        user = None
    
    return render(request, 'evidence/depart.html', {'user': user, 'user': user, 'depart': depart, 'comments': comments, 'sets': sets, 'comment_form': comment_form})


def departsearch(request):

    try:
        user = User.objects.get(pk=request.session['user'])
    except User.DoesNotExist and KeyError:
        user = None
    
    return render(request, 'evidence/departsearch.html', {'user': user})


def departlivesearch(request, word, sid1, sid2):
    departs = Depart.objects.filter(line__icontains=word)

    """
    if len(departs) > 0:
        response = ''
        for d in departs:
            response += f'{reverse("depart", args=(d.id,))},{d.line},{d.route},{d.date.strftime("%d.%m.%Y")}|'
    else:
        response = ''
    """
    
    return render(request, 'evidence/departlivesearch.html', {'departs': departs})


def addcomment(request, depart_id):
    depart = get_object_or_404(Depart, pk=depart_id)
    if request.method == 'POST':
        comment_form = CommentForm(request.POST)
        if comment_form.is_valid():
            try:
                user = User.objects.get(pk=request.session['user'])
            except User.DoesNotExist and KeyError:
                user = None
    
            if user == None:
                messages.error(request, "You need to be logged in to add message!")
                return redirect('depart', depart_id=depart_id)

            comment_content = comment_form.cleaned_data['content']
            
            comment = Comment(depart=depart, content=comment_content, user=user)
            comment.save()
            
            return redirect('depart', depart_id=depart_id)


def user(request, user_id):
    user2 = get_object_or_404(User, pk=user_id)
    comments = Comment.objects.filter(user=user2)

    try:
        user = User.objects.get(pk=request.session['user'])
    except User.DoesNotExist and KeyError:
        user = None
    
    return render(request, 'evidence/user.html', {'user': user, 'user2': user2, 'comments': comments})


def usersearch(request):

    users = None
    if request.method == 'POST':
        form = UserSearchForm(request.POST)
        if form.is_valid():
            users = User.objects.filter(username__icontains=form.cleaned_data['username'])

    try:
        user = User.objects.get(pk=request.session['user'])
    except User.DoesNotExist and KeyError:
        user = None

    form = UserSearchForm()
    return render(request, 'evidence/usersearch.html', {'user': user, 'users': users, 'form': form})

def stopsearch(request):

    stops = None
    if request.method == 'POST':
        form = StopSearchForm(request.POST)
        if form.is_valid():
            stops = Stop.objects.filter(name__icontains=form.cleaned_data['name'])

    try:
        user = User.objects.get(pk=request.session['user'])
    except User.DoesNotExist and KeyError:
        user = None

    form = StopSearchForm()
    return render(request, 'evidence/stopsearch.html', {'user': user, 'stops': stops, 'form': form})


def vehicle(request, vehicle_id):
    vehicle = get_object_or_404(Vehicle, pk=vehicle_id)
    sets = SetVehicle.objects.filter(vehicle=vehicle)
    reg_nums = RegNumberHistory.objects.filter(vehicle=vehicle)

    try:
        user = User.objects.get(pk=request.session['user'])
    except User.DoesNotExist and KeyError:
        user = None
    
    return render(request, 'evidence/vehicle.html', {'user': user, 'vehicle': vehicle, 'sets': sets, 'reg_nums': reg_nums})

def vehiclesearch(request):

    vehicles = None
    if request.method == 'POST':
        form = VehicleSearchForm(request.POST)
        if form.is_valid():
            print(form.cleaned_data['reg_num'] if form.cleaned_data['reg_num'] is not None else "Noooone")
            c_valid = Q(carrier=form.cleaned_data['carrier']) if form.cleaned_data['carrier'] is not None else Q(pk__gt=0)
            t_valid = Q(type=form.cleaned_data['type']) if form.cleaned_data['type'] is not None else Q(pk__gt=0)
            r_valid = Q(reg_num__icontains=form.cleaned_data['reg_num']) if len(form.cleaned_data['reg_num']) != 0 else Q(pk__gt=0)
            vehicles = Vehicle.objects.filter(c_valid & t_valid & r_valid)

    try:
        user = User.objects.get(pk=request.session['user'])
    except User.DoesNotExist and KeyError:
        user = None

    form = VehicleSearchForm()
    return render(request, 'evidence/vehiclesearch.html', {'user': user, 'vehicles': vehicles, 'form': form})

def vehicletypes(request):
    veh_types = VehicleType.objects.all().order_by('manufacturer')

    try:
        user = User.objects.get(pk=request.session['user'])
    except User.DoesNotExist and KeyError:
        user = None

    return render(request, 'evidence/vehtypes.html', {'user': user, 'veh_types': veh_types})