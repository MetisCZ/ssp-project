from django import forms
from django.contrib.auth.hashers import check_password
from django.contrib.auth.forms import UserCreationForm
from .models import Comment, User, Carrier, VehicleType

# + depart search form (using Ajax and JS)

class UserLoginForm(forms.Form):
    username = forms.CharField(max_length=200, label='Username')
    password = forms.CharField(widget=forms.PasswordInput, label='Password')

    def clean(self):
        cleaned_data = super().clean()
        username = cleaned_data.get('username')
        password = cleaned_data.get('password')

        try:
            user = User.objects.get(username=username)
        except User.DoesNotExist:
            raise forms.ValidationError('Invalid login name or password.')

        if not check_password(password, user.password_hash):
            raise forms.ValidationError('Invalid login name or password.')

        cleaned_data['user'] = user
        return cleaned_data

class UserRegistrationForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ['content']


class UserSearchForm(forms.Form):
    username = forms.CharField(label="Username", max_length=24, required=False)


class StopSearchForm(forms.Form):
    name = forms.CharField(label="Name", max_length=64, required=False)

class VehicleSearchForm(forms.Form):
    reg_num = forms.CharField(label="Reg. num", max_length=7, required=False)
    carrier = forms.ModelChoiceField(queryset=Carrier.objects.all(), required=False)
    type = forms.ModelChoiceField(queryset=VehicleType.objects.all(), required=False)