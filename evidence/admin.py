from django.contrib import admin
from .models import *

admin.site.register(Vehicle)
admin.site.register(Depart)
admin.site.register(Comment)
admin.site.register(Manufacturer)
admin.site.register(VehicleType)
admin.site.register(Stop)
admin.site.register(User)
admin.site.register(Carrier)
admin.site.register(SetVehicle)
admin.site.register(RegNumberHistory)
