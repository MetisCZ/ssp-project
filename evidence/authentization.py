from django.contrib.auth.backends import BaseBackend
from django.contrib.auth.hashers import check_password

from .models import User

class UserBackend(BaseBackend):
    def authenticate(self, request, username=None, password=None):
        user = User.objects.filter(username=username)
        if len(user) == 0:
            return None
        if not check_password(password, user[0].password_hash):
            return None
        return user[0]
    
    def get_user(self, user_id):
        user = User.objects.filter(pk=user_id)
        if len(user) == 0:
            return None
        return user[0]