# Generated by Django 4.2 on 2023-04-22 19:38

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Carrier',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=32)),
            ],
        ),
        migrations.CreateModel(
            name='Depart',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('line', models.CharField(max_length=6)),
                ('route', models.IntegerField()),
                ('date', models.DateField(auto_now_add=True)),
                ('datetime_start', models.DateTimeField(auto_now_add=True)),
                ('datetime_actual', models.DateTimeField(auto_now_add=True)),
                ('actual_delay', models.IntegerField(blank=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Manufacturer',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=32)),
            ],
        ),
        migrations.CreateModel(
            name='Stop',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=64)),
            ],
        ),
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nickname', models.CharField(max_length=24)),
                ('email', models.EmailField(max_length=320)),
                ('role', models.SmallIntegerField(default=0)),
                ('password_hash', models.CharField(max_length=256)),
                ('last_login', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='VehicleType',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=32)),
                ('manufacturer', models.ForeignKey(on_delete=django.db.models.deletion.RESTRICT, to='evidence.manufacturer')),
            ],
        ),
        migrations.CreateModel(
            name='Vehicle',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('reg_num', models.CharField(max_length=7)),
                ('state', models.SmallIntegerField()),
                ('datetime_added', models.DateTimeField(auto_now_add=True, verbose_name='date added')),
                ('manufactured_year', models.SmallIntegerField(blank=True, null=True)),
                ('last_seen', models.DateTimeField(blank=True, null=True, verbose_name='last seen date')),
                ('SPZ', models.CharField(blank=True, max_length=7, null=True)),
                ('carrier', models.ForeignKey(on_delete=django.db.models.deletion.RESTRICT, to='evidence.carrier')),
                ('type', models.ForeignKey(on_delete=django.db.models.deletion.RESTRICT, to='evidence.vehicletype')),
            ],
        ),
        migrations.CreateModel(
            name='SetVehicle',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('position', models.SmallIntegerField()),
                ('depart', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='evidence.depart')),
                ('vehicle', models.ForeignKey(on_delete=django.db.models.deletion.RESTRICT, to='evidence.vehicle')),
            ],
        ),
        migrations.CreateModel(
            name='RegNumberHistory',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_valid_from', models.DateTimeField()),
                ('date_valid_to', models.DateTimeField(auto_now_add=True)),
                ('reg_num', models.CharField(max_length=7)),
                ('vehicle', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='evidence.vehicle')),
            ],
        ),
        migrations.AddField(
            model_name='depart',
            name='actual_stop',
            field=models.ForeignKey(on_delete=django.db.models.deletion.RESTRICT, related_name='astop', to='evidence.stop'),
        ),
        migrations.AddField(
            model_name='depart',
            name='first_stop',
            field=models.ForeignKey(on_delete=django.db.models.deletion.RESTRICT, related_name='fstop', to='evidence.stop'),
        ),
        migrations.AddField(
            model_name='depart',
            name='first_vehicle',
            field=models.ForeignKey(on_delete=django.db.models.deletion.RESTRICT, to='evidence.vehicle'),
        ),
        migrations.AddField(
            model_name='depart',
            name='last_stop',
            field=models.ForeignKey(on_delete=django.db.models.deletion.RESTRICT, related_name='lstop', to='evidence.stop'),
        ),
        migrations.CreateModel(
            name='Comment',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('datetime_added', models.DateTimeField(auto_now_add=True)),
                ('content', models.TextField()),
                ('depart', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='evidence.depart')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='evidence.user')),
            ],
        ),
    ]
