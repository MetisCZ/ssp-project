
var xmlhttp;

function showResult(str)
{
    if(str.length == 0)
    {
        document.getElementById("livesearch").innerHTML = "";
        return;
    }

    xmlhttp = new XMLHttpRequest();

    let url = "/depart/search";
    url += "/" + str;
    url += "/" + Math.random();

    xmlhttp.onreadystatechange = stateChange;
    xmlhttp.open("GET", url, true);
    xmlhttp.send(null);
}

function stateChange()
{
    if(xmlhttp.readyState == 4)
    {
        /*let html = '';
        let departs = xmlhttp.responseText.split('|');
        departs.forEach(depart => {
            data = depart.split(',');
            if(data.length === 4)
            {
                html += `<p><a href='${data[0]}'>${data[1]}/${data[2]} at ${data[3]}</a></p>`;
            }
        });*/
        document.getElementById("livesearch").innerHTML = xmlhttp.responseText;
    }
}